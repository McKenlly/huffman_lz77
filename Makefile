FLAGS=-std=c++11 -pedantic -O2
CC=g++
LINK=-lm

all: main

main: main.cpp
	$(CC) $(FLAGS) -o DA_KP main.cpp src/bitsBuffer/bitsBuffer.cpp src/bitsBuffer/bitsBuffer.h src/huffman/huffman.cpp src/huffman/huffman.h src/lz77/LZ77.cpp src/lz77/LZ77.hpp src/errors.hpp src/file_util.hpp

