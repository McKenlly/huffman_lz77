#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <dirent.h>
#include <iterator>
#include <string.h>

#include "src/huffman/huffman.h"
#include "src/errors.hpp"
#include "src/file_util.hpp"
#include "src/lz77/LZ77.hpp"

Error Compress(std::string &filename, Option& opt) {
    std::ifstream inputData;
    std::ofstream outputTmp;
    std::ifstream inputTmp;
    std::ofstream outputData;
    uint32_t maxCodeLen;
    if (opt.labelFastest) {
        maxCodeLen = 16;
    } else {
        maxCodeLen = 12;
    }
    std::string tmpname = filename + "~.tmp";
//    std::string tmpname = filename + "~.Z";

    outputTmp.open(tmpname, std::ofstream::out | std::ofstream::binary);

    if (!(outputTmp.is_open() && outputTmp.good())) {
        std::cerr << "Error while creating temporary file" << std::endl;
        outputTmp.close();
        return FILE_COMPRESS_FAIL;
    }

    if (opt.labelReadFromStdIn) {
        std::istream &inputStd = std::cin;
        LZ77::encode(inputStd, outputTmp, maxCodeLen);
//        TreeNode::EncodeHuffman(inputTmp, inputStd);
        outputTmp.close();
    } else {
        inputData.open(filename, std::ifstream::in | std::ifstream::binary);
        if (!(inputData.is_open())) {
            std::cerr << "Error while opening file " << filename << std::endl;
            inputData.close();
            return FILE_COMPRESS_FAIL;

        }
//        TreeNode::EncodeHuffman(inputData, outputTmp);

        LZ77::encode(inputData, outputTmp, maxCodeLen);

        outputTmp.close();
        inputData.close();
    }

    //2 часть
    inputTmp.open(tmpname, std::ifstream::in | std::ifstream::binary);
    if (!(inputTmp.is_open())) {
        std::cerr << "Error while opening temporary file" << std::endl;
        inputTmp.close();
        return FILE_COMPRESS_FAIL;
    }
    if (opt.labelWriteToStdOut) {
        uint8_t byte = 0xee;
        std::cout.write((char *) &byte, 1);
        TreeNode::EncodeHuffman(inputTmp, std::cout);
        inputTmp.close();
    } else {
        outputData.open(filename + ".Z", std::ofstream::out | std::ofstream::binary);
        if (!(outputData.is_open() && outputData.good())) {
            std::cerr << "Error while creating archive file" << std::endl;
            outputData.close();
            return FILE_COMPRESS_FAIL;
        }
        uint8_t specialByte = 0xee;
        outputData.write((char *) &specialByte, 1);

        TreeNode::EncodeHuffman(inputTmp, outputData);
        //write down uncompressed size
        outputData.write((char *) &specialByte, 1);
        uint64_t uncompressedSize = getFileSize(filename);
        outputData.write((char *) &uncompressedSize, 8);
        inputTmp.close();
        outputData.close();
    }
    if (remove(tmpname.data())) {
        std::cerr << "Error while removing temporary file" << std::endl;
        return FILE_COMPRESS_FAIL;
    }
    //TODO DELETE Source File if this exist and not option -k(https://linux-faq.ru/page/komanda-gzip)
    if (!opt.labelKeepFiles && !opt.labelReadFromStdIn) {
        if (remove(filename.data())) {
            std::cerr << "Error while removing file " << filename << std::endl;
            return FILE_COMPRESS_FAIL;
        }
    }
    return FILE_COMPRESS_SUCCESS;
}
Error Decompress(std::string &filename, Option& opt) {
    std::ifstream inputData;
    std::ofstream outputTmp;
    std::ifstream inputTmp;
    std::ofstream outputData;
    uint32_t ret = 0;
    std::string tmpname = filename + "~.tmp";
    std::string unpackedname = "";
    outputTmp.open(tmpname, std::ofstream::out | std::ofstream::binary);
    if (!(outputTmp.is_open() && outputTmp.good())) {
        std::cerr << "Error while creating temporary file" << std::endl;
        outputTmp.close();
        return FILE_DECOMPRESS_FAIL;
    }
    if (opt.labelReadFromStdIn) {
        uint8_t specialByte = 0xee;
        uint8_t byte;
        inputData.read((char *) &byte, 1);
        if (byte == specialByte) {
            TreeNode::DecodeHuffman(std::cin, outputTmp);
        } else {
            ret = 1;
        }
        outputTmp.close();
    } else {
        unpackedname = filename.substr(0, filename.length() - 2);
        if (!isFileExistSuffics(std::cerr, filename)) {
            return FILE_DECOMPRESS_FAIL;
        }
        inputData.open(filename, std::ifstream::in | std::ifstream::binary);
        if (IsFileOpen(inputData) == FILE_CLOSE) {
            std::cerr << "Error while opening archive file " << filename << std::endl;
            inputData.close();
            return FILE_DECOMPRESS_FAIL;
        }
        uint8_t specialByte = 0xee;
        uint8_t byte;
        inputData.clear();
        inputData.read((char *) &byte, 1);
        if (byte == specialByte) {
            TreeNode::DecodeHuffman(inputData, outputTmp);
        } else {
            ret = FILE_DECOMPRESS_FAIL;
        }
        inputData.close();
        outputTmp.close();
    }
    if (ret) {
        return FILE_DECOMPRESS_FAIL;
    }

    inputTmp.open(tmpname, std::ifstream::in | std::ifstream::binary);

    if (!(inputTmp.is_open())) {
        std::cerr << "Error while opening temporary file" << std::endl;
        inputTmp.close();
        return FILE_DECOMPRESS_FAIL;
    }
    if (opt.labelWriteToStdOut) {
        std::ostream &outputStd = std::cout;
        LZ77::decode(inputTmp, outputStd);
        inputTmp.close();
    } else {
        outputData.open(unpackedname, std::ofstream::out | std::ofstream::binary);
        if (!(outputData.is_open() && outputData.good())) {
            std::cerr << "Error while creating file: " << unpackedname << std::endl;
            outputData.close();
            return FILE_DECOMPRESS_FAIL;
        }
        LZ77::decode(inputTmp, outputData);
        outputData.close();
        inputTmp.close();
    }
    if (remove(tmpname.data())) {
        std::cerr << "Error while removing temporary file" << std::endl;
        return FILE_DECOMPRESS_FAIL;
    }
    if (!opt.labelKeepFiles && !opt.labelReadFromStdIn) {
        if (remove(filename.data())) {
            std::cerr << "Error while removing file " << filename << std::endl;
            return FILE_DECOMPRESS_FAIL;
        }
    }
    if (ret) {
        std::cerr << "Wrong archive structure:" << std::endl;
    }
    return ret == 0 ? FILE_DECOMPRESS_SUCCESS : FILE_DECOMPRESS_FAIL;
}

void checkIntegrity(const std::string &filename) {
    if (!isFileExistSuffics(std::cerr, filename))  {
        return;
    }
    std::ifstream inputData;
    inputData.open(filename, std::ifstream::in | std::ifstream::binary);
    uint8_t byte;
    bool corruption = false;
    inputData.read((char *) &byte, 1);
    if (byte != 0xee)
        corruption = true;
    else {
        inputData.seekg(-9, inputData.end);
        inputData.read((char *) &byte, 1);
        if (byte != 0xee)
            corruption = true;
    }
    inputData.close();
    if (corruption)
        std::cout << "Archive file " << filename << " corrupted" << std::endl;
    else
        std::cout << "Archive file " << filename << " is consistent" << std::endl;
}
/*FINISH*/
void getInfo(const std::string &filename);

int  RecursiveByDir(Option& opt, const std::string& filename);

int main(int argc, char const *argv[]) {

    Option mOption;
    getOptions(mOption, argc, argv);

    std::string filename = argv[argc - 1];

    if (filename == "-") {
        setLabelStdOptions(mOption, true, true);
    }
    //default
    mOption.labelCompress = isLabelCompress(mOption);
    //recursive option
    if (mOption.labelRecursive) {
        int ans = RecursiveByDir(mOption, filename);
        return ans;
    }
    if (mOption.labelCompress) {
        return Compress(filename, mOption);
    } else if (mOption.labelDecompress) {
        return Decompress(filename, mOption);
    } else if (mOption.labelListProperties && !mOption.labelReadFromStdIn) {
        getInfo(filename);
    } else if (mOption.labelTestIntegrity) {
        checkIntegrity(filename);
    }
    return EXIT_SUCCESS;
}

void getInfo(const std::string &filename) {
    if (isFileExistSuffics(std::cerr, filename)) {
        switch (IsFileExist(filename)) {
            case FILE_EXIST: {
                printInfoMessageByFile(filename, std::cout);
                break;
            }
            case FILE_DOES_NOT_EXIST: {
                printError(std::cerr);
                break;
            }
            default: {
                printUnknownAccess(std::cerr);
            }
        }
    }
}

int  RecursiveByDir(Option& opt, const std::string& filename) {
    std::vector<std::string> files;
    DIR *pDIR;
    struct dirent *entry;
    if(pDIR = opendir(filename.c_str())){
        //TODO push all files from dir
        while(entry = readdir(pDIR)){
            if(strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
                std::string ch =  entry->d_name;
                ch = filename + "/" + ch;
                DIR *dp = opendir(ch.c_str());
                if (dp == NULL) {
                    std::cout << entry->d_name << "\n";
                    files.push_back(filename + "/" + entry->d_name);

                } else {
                    closedir(dp);
                }
            }
        }
        closedir(pDIR);
        free(entry);
    }

    if (opt.labelCompress) {
        for (std::string &file : files) {
            setLabelStdOptions(opt, false, false);
            if (Compress(file, opt) == FILE_COMPRESS_FAIL)
                return EXIT_FAILURE;
        }
    } else if (opt.labelDecompress) {
        for (std::string &file : files) {
            setLabelStdOptions(opt, false, false);
            if (Decompress(file, opt) == FILE_DECOMPRESS_FAIL)
                return EXIT_FAILURE;
        }
    } else if (opt.labelListProperties && !opt.labelReadFromStdIn) {
        getInfo(filename);
    } else if (opt.labelTestIntegrity) {
        checkIntegrity(filename);
    }
    return EXIT_SUCCESS;
}
