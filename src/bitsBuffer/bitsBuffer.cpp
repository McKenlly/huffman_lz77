#include "bitsBuffer.h"

BitsInputBuffer::BitsInputBuffer(std::istream &file) : file(file) {
}

void BitsInputBuffer::Flush() {
    buffer = 0;
    bufferCount = 0;
}

bool BitsInputBuffer::Empty() {
    file.peek();
    return (bufferCount == 0) && file.eof();
}

uint32_t BitsInputBuffer::GetBits(uint32_t len) {
    uint32_t symbol = 0;
    if (bufferCount < len)
        ReadBits(len);
    //still not enough
    if (bufferCount < len) {
        //input data ended
        //return what have
        uint32_t ret = buffer;
        file.peek();
        buffer = 0;
        bufferCount = 0;
        return ret;
    }
    //get len bits from begin of bufferCount
    bufferCount -= len;
    symbol = (uint32_t) (buffer >> bufferCount);
    //delete first len bits stored
    if (bufferCount == 0) {
        //shifts for full size do not work
        //left shift count >= width of type [-Wshift-count-overflow]
        symbol = buffer;
        buffer = 0;
    } else {
        buffer = buffer << (64 - bufferCount);
        buffer = buffer >> (64 - bufferCount);
    }
    return symbol;
}

void BitsInputBuffer::ReadBits(uint32_t len) {
    uint8_t byte = 0;
    while (bufferCount < len) {
        byte = 0;
        file.read((char *) &byte, 1);
        buffer = (buffer << 8) | (uint64_t) (byte);
        bufferCount += 8;
        file.peek();
        //to trigger eof
        if (file.eof()) {
            return;
        }
    }
}

BitsOutputBuffer::BitsOutputBuffer(std::ostream &outputFile) : file(outputFile) {}

void BitsOutputBuffer::AddToBuffer(uint64_t code, int len) {
    buffer = (buffer << len) | (uint64_t) code;
    bufferCount += len;
    if (bufferCount >= 24)
        WriteBits();
}

void BitsOutputBuffer::AddToBuffer(std::vector<bool> &code) {
    for (bool k : code) {
        //put in code
        if (k)
            buffer = (buffer << 1) | 1;
        else
            buffer = (buffer << 1);
        ++bufferCount;
        if (bufferCount >= 32)
            WriteBits();
    }
}


void BitsOutputBuffer::AddToBuffer(uint16_t code) {
    buffer = (buffer << 16) | (uint64_t) code;
    bufferCount += 16;
    WriteBits();
}

void BitsOutputBuffer::AddToBuffer(bool code) {
    if (code)
        buffer = (buffer << 1) | 1;
    else
        buffer = (buffer << 1);
    ++bufferCount;
    if (bufferCount >= 24) {
        WriteBits();
    }
}

void BitsOutputBuffer::Flush() {
    WriteBits();
    uint8_t byte = 0;
    //fill with 0 empty space
    if (bufferCount == 0) {
        //no incomplete bytes
        //so no filler needed
        return;
    }
    byte = (uint8_t) (buffer << (8 - bufferCount));
    file.write((char *) &byte, 1);
    buffer = 0;
    bufferCount = 0;
}
void BitsOutputBuffer::WriteBits() {
    uint8_t byte = 0;
    while (bufferCount >= 8) {
        bufferCount -= 8;
        byte = (uint8_t) (buffer >> (bufferCount));
        if (bufferCount == 0) {
            buffer = 0;
        } else {
            buffer = buffer << (64 - bufferCount);
            buffer = buffer >> (64 - bufferCount);
        }
        file.write((char *) &byte, 1);
    }
}
