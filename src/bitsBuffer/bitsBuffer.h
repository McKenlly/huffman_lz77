#ifndef BITSBUFFER_HPP
#define BITSBUFFER_HPP

#include <fstream>
#include <iostream>
#include <vector>
#include <algorithm>

class BitsInputBuffer {
public:
    BitsInputBuffer(std::istream &file);
    void Flush();
    bool Empty();
    uint32_t GetBits(uint32_t len);

private:
    void ReadBits(uint32_t len);
    std::istream &file;
    uint64_t buffer = 0;
    uint32_t bufferCount = 0;
};

class BitsOutputBuffer {
public:
    BitsOutputBuffer(std::ostream &outputFile);

    void AddToBuffer(uint64_t code, int len);

    void AddToBuffer(std::vector<bool> &code);


    void AddToBuffer(uint16_t code);

    void AddToBuffer(bool code);

    void Flush();
    virtual ~BitsOutputBuffer() {}

private:
    std::ostream &file;
    uint64_t buffer = 0;
    uint32_t bufferCount = 0;

    void WriteBits();
};

#endif