//
// Created by mckenlly on 10/1/18.
//

#ifndef DA_KP_ERRORS_HPP
#define DA_KP_ERRORS_HPP

#include <string>
#include <iostream>
#include <sys/stat.h>
#include <fstream>

enum Error {
    FILE_EXIST = 0,
    FILE_OPEN = 2,
    ERROR_BINARY = 3,
    FILE_CLOSE = 4,
    FILE_DOES_NOT_EXIST = -1,
    FILE_COMPRESS_FAIL = -2,
    FILE_COMPRESS_SUCCESS = 5,
    FILE_DECOMPRESS_SUCCESS = 6,
    FILE_DECOMPRESS_FAIL = -3,
    };
Error IsFileExist(const std::string& filename) {
    struct stat buffer;
    return Error(stat (filename.c_str(), &buffer));
}
Error IsFileOpen(std::ifstream& stream) {
    return (stream.is_open() == true ? FILE_OPEN : FILE_CLOSE);
}
#endif //DA_KP_ERRORS_HPP
