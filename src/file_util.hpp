//
// Created by mckenlly on 10/1/18.
//

#ifndef DA_KP_FILE_UTIL_HPP
#define DA_KP_FILE_UTIL_HPP

#include <ostream>
#include <fstream>
#include <sys/stat.h>
#include "errors.hpp"

void printError(std::ostream & ostream) {
    ostream << "Sorry, file does not exist";
}

void printUnknownAccess(std::ostream & ostream) {
    ostream << "Sorry, unknown error";
}
uint64_t getFileSize(const std::string& filename) {
        struct stat file_stat;
        stat(filename.c_str(), &file_stat);
        return file_stat.st_size;
}
void printInfoMessageByFile(const std::string& fileName, std::ostream& ostream) {
    ostream << fileName << ":" << std::endl;
    std::string unpackedname = fileName.substr(0, fileName.length() - 2);
    uint64_t uncompressedSize = 0;
    uint64_t compressedSize = getFileSize(fileName);

    ostream << "\tcompressed " << compressedSize<< std::endl;
    std::ifstream inputData;
    inputData.open(fileName, std::ifstream::in | std::ifstream::binary);
    if (IsFileOpen(inputData) == FILE_OPEN) {
        ostream << "Error while opening file " << fileName << std::endl;
        inputData.close();
    }
    inputData.seekg(-8, inputData.end);
    inputData.read((char *) &uncompressedSize, 8);
    inputData.close();
    ostream << "\tuncompressed " << uncompressedSize << std::endl;
    ostream << "\tuncompressed_name " << unpackedname << std::endl;
}

bool isFileExistSuffics(std::ostream& stream, const std::string& fileName) {
    if (fileName.substr(fileName.length() - 2, fileName.length() - 1) != ".Z") {
        stream << fileName << "Sorry, is not a .Z archive" << std::endl;
        return false;
    }
    return true;
}
struct Option {
    bool labelWriteToStdOut = false;
    bool labelReadFromStdIn = false;
    bool labelDecompress = false;
    bool labelKeepFiles = false;
    bool labelListProperties = false;
    bool labelRecursive = false;
    bool labelTestIntegrity = false;
    bool labelFastest = true;
    //default -9
    bool labelCompress = false;

};
bool isLabelCompress(Option &opt) {
    return (!opt.labelTestIntegrity && !opt.labelDecompress && !opt.labelListProperties);
}
void getOptions(Option& opt, const int& argc, char const *argv[]) {
    for (int i = 1; i < argc - 1; ++i) {
        std::string arg = argv[i];
        if (arg == "-c") {
            opt.labelWriteToStdOut = true;
        } else if (arg == "-d") {
            opt.labelDecompress = true;
        } else if (arg == "-k") {
            opt.labelKeepFiles = true;
        } else if (arg == "-l") {
            opt.labelListProperties = true;
        } else if (arg == "-r") {
            opt.labelRecursive = true;
        } else if (arg == "-t") {
            opt.labelTestIntegrity = true;
        } else if (arg == "-1") {
            opt.labelFastest = true;
        } else if (arg == "-9") {
            opt.labelFastest = false;
        }
    }
}
void setLabelStdOptions(Option& opt, bool out, bool in) {
    opt.labelReadFromStdIn = in;
    opt.labelWriteToStdOut = out;
}

#endif //DA_KP_FILE_UTIL_HPP
