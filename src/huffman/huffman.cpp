#include "huffman.h"


TreeNode::TreeNode(uint16_t sym, uint32_t prior) {
    left = nullptr;
    right = nullptr;
    symbol = sym;
    priority = prior;
    isLeaf = true;
}

TreeNode::TreeNode(TreeNode *leftNode, TreeNode *rightNode) {
    left = leftNode;
    right = rightNode;
    symbol = 0;
    priority = leftNode->priority + rightNode->priority;
    isLeaf = false;
}

TreeNode::TreeNode(uint16_t sym) {
    left = nullptr;
    right = nullptr;
    symbol = sym;
    priority = 0;
    isLeaf = true;
}

TreeNode::TreeNode() {
    left = nullptr;
    right = nullptr;
    symbol = 0;
    priority = 0;
    isLeaf = false;
}

void TreeNode::DeepFirstSearch(const TreeNode *node, std::vector<std::vector<bool>> &table, std::vector<bool> &path) {
    if (node->isLeaf) {
        table[node->symbol] = path;
        return;
    }
    path.push_back(false);

    DeepFirstSearch(node->left, table, path);
    path[path.size() - 1] = true;
    DeepFirstSearch(node->right, table, path);

    path.pop_back();
}


void TreeNode::DFSSerialize(const TreeNode *node, BitsOutputBuffer &buffer) {
    if (node->isLeaf) {
        buffer.AddToBuffer(true);
        buffer.AddToBuffer(node->symbol);
        return;
    }
    buffer.AddToBuffer(false);
    DFSSerialize(node->left, buffer);
    DFSSerialize(node->right, buffer);
}


TreeNode *TreeNode::DFSDeserialize(BitsInputBuffer &buffer) {
    if (buffer.GetBits(1)) {
        //isLeaf
        return new TreeNode((uint16_t) buffer.GetBits(16));
    } else {
        TreeNode *node = new TreeNode();
        node->left = DFSDeserialize(buffer);
        node->right = DFSDeserialize(buffer);
        return node;
    }
}

void TreeNode::CreateHuffmanTree(std::vector<uint32_t>& pr, std::priority_queue<TreeNode *, std::vector<TreeNode *>, TreeNode::Comp>& pq) {
    for (uint16_t i = 0; i < pr.size(); i++) {
        if (pr[i])
            pq.push(new TreeNode(i, pr[i]));
    }

    pq.push(new TreeNode(TreeNode::EXIT_CODE, 0));
    //TODO nothing clear
    pr.clear();

    TreeNode *left, *rigth;
    //TODO Tree Huffman
    while (pq.size() > 1) {
        left = pq.top(); pq.pop();
        rigth = pq.top(); pq.pop();
        pq.push(new TreeNode(left, rigth));
    }
}

int TreeNode::EncodeHuffman(std::istream &inputData, std::ostream &outputData) {
    //TODO Vector with amount all symbols
    std::vector<uint32_t> priorityVecSym(TreeNode::EXIT_CODE, 0);
    std::priority_queue<TreeNode *, std::vector<TreeNode *>, TreeNode::Comp> pq;
    uint8_t byte;
    /*TODO Preprocessing*/
    while (!inputData.eof()) {
        inputData.read( (char * )&byte, 1);
        //TODO Error
        priorityVecSym[byte]++;
        inputData.peek();
    }

    CreateHuffmanTree(priorityVecSym, pq);
    //GET ROOT
    TreeNode *root = pq.top();
    //clear queue
    pq.pop();
    std::vector<std::vector<bool> > codeTable(TreeNode::EXIT_CODE + 2);
    std::vector<bool> path;
    DeepFirstSearch(root, codeTable, path);
    //TODO CLEAR EMPTY VALUES
    codeTable.shrink_to_fit();


    //no more changes in codeTable
    BitsOutputBuffer outputBuffer(outputData);
    DFSSerialize(root, outputBuffer);
    delete root;
    //tree no longer needed - there is codeTable
    //second run through file
    inputData.clear();
    //TODO ?
    inputData.seekg(0, std::ios::beg);
    //seekg == seek get position
    //zero is offset from position
    while (!inputData.eof()) {
        inputData.read((char *) &byte, 1);
        outputBuffer.AddToBuffer(codeTable[byte]);
        inputData.peek();
    }
    outputBuffer.AddToBuffer(codeTable[TreeNode::EXIT_CODE]);
    outputBuffer.Flush();
    codeTable.clear();
}

void TreeNode::DecodeHuffman(std::istream &encodedData, std::ostream &decodedData) {
    BitsInputBuffer inputBuffer(encodedData);
    //get tree from file
    TreeNode *root = DFSDeserialize(inputBuffer);
    TreeNode *currNode = root;
    while (!inputBuffer.Empty()) {
        if (inputBuffer.GetBits(1))
            currNode = currNode->right;
        else
            currNode = currNode->left;
        if (currNode->isLeaf) {
            if (currNode->symbol == TreeNode::EXIT_CODE)
                return;
            decodedData.write((char *) &currNode->symbol, 1);
            currNode = root;
        }
    }
}

TreeNode::~TreeNode() {
    if (!isLeaf) {
        delete left;
        delete right;
    }
}

