//
// Created by mckenlly on 9/30/18.
//

#ifndef DA_KP_HUFFMAN_H
#define DA_KP_HUFFMAN_H

#include <fstream>
#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <bitset>
#include "../bitsBuffer/bitsBuffer.h"

class TreeNode {
public:
    static const  uint16_t EXIT_CODE = 256;
    //Encoding
    explicit TreeNode(uint16_t sym, uint32_t prior);
    //Decoding
    explicit TreeNode(uint16_t sym);
    explicit TreeNode(TreeNode *leftNode, TreeNode *rightNode);
    explicit TreeNode();

    /*Comparator*/
    struct Comp {
        bool operator()(const TreeNode *a, const TreeNode *b) {
            return a->priority > b->priority;
        }
    };

    static void DeepFirstSearch(const TreeNode *node, std::vector<std::vector<bool>> &table, std::vector<bool> &path);
    static void DFSSerialize(const TreeNode *node, BitsOutputBuffer &buffer);
    static TreeNode *DFSDeserialize(BitsInputBuffer &buffer);
    /*TODO 0 errors*/
    static int EncodeHuffman(std::istream &inputData, std::ostream &outputData);
    static void DecodeHuffman(std::istream &encodedData, std::ostream &decodedData);

    virtual ~TreeNode();
private:
    static void CreateHuffmanTree(std::vector<uint32_t>&, std::priority_queue<TreeNode*, std::vector<TreeNode *>, Comp>&);
    TreeNode *left;
    TreeNode *right;
    uint16_t symbol;
    uint32_t priority;
    bool isLeaf;
};

#endif
