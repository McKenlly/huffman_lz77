//
// Created by mckenlly on 10/2/18.
//

#include <deque>
#include <fstream>
#include <cmath>
#include "LZ77.hpp"
#include "../bitsBuffer/bitsBuffer.h"




void LZ77::encode(std::istream &inputData, std::ostream &outputData, uint16_t windowSize, uint16_t maxLength) {
    windowSize = 32 * 1024;
    maxLength = 256;
    uint64_t bufferLength = maxLength * maxLength;
    std::deque<uint16_t> preBuffer;
    std::deque<uint16_t> dictionary;
    BitsOutputBuffer bitsOutputBuffer(outputData);
    bitsOutputBuffer.AddToBuffer(windowSize, 16);

    char byte;
    while (!inputData.eof()) {

        for (auto i = 0; (i < bufferLength) && !inputData.eof(); i++) {
            inputData.read(&byte, 1);
            preBuffer.push_back((uint16_t)byte);
            //check for eof
            inputData.peek();

        }

        while (!preBuffer.empty()) {

            uint16_t index = 0;
            uint16_t length = 0;
            //поиск в словаре самой большой подстроки
            for (uint16_t dictPos = 0; (dictPos < dictionary.size()); dictPos++) {
                uint16_t tempIndex = 0;
                uint16_t tempLength = 0;

                for (uint16_t prePos = 0; prePos < preBuffer.size(); prePos++) {
                    if (dictPos - prePos < 0) {
                        break;
                    }
                    if (dictionary[dictPos - prePos] == preBuffer[prePos]) {

                        tempLength = prePos;
                        tempIndex = dictPos;

                        if (tempLength == maxLength - 1) {
                            break;
                        }
                    } else {
                        break;
                    }
                }

                if (tempLength > length) {
                    index = tempIndex;
                    length = tempLength;
                    if (tempLength == maxLength - 1) {
                        break;
                    }
                }
            }

            if (length > 0) {
                bitsOutputBuffer.AddToBuffer(index, 16);
                bitsOutputBuffer.AddToBuffer(length, 16);
                bitsOutputBuffer.AddToBuffer(preBuffer[length]);

                for (auto i = 0; i < length + 1; i++) {
                    dictionary.push_front(preBuffer[0]);
                    preBuffer.pop_front();
                    if (dictionary.size() > windowSize) {
                        dictionary.pop_back();
                    }
                }
            } else {
                bitsOutputBuffer.AddToBuffer(index, 16);
                //если длинна равна нулю нет смысла записывать
                //bitsOutputBuffer.AddToBuffer(length, 16);
                bitsOutputBuffer.AddToBuffer(preBuffer[0]);

                //добавляем символ
                dictionary.push_front(preBuffer[0]);
                preBuffer.pop_front();

                while (dictionary.size() > windowSize) {
                    dictionary.pop_back();
                }
            }
            if (preBuffer.size() < maxLength && !inputData.eof()) {
                break;
            }
        }

    }

    dictionary.clear();
    preBuffer.clear();

}


void LZ77::decode(std::istream &input, std::ostream &output) {

    BitsInputBuffer buffer(input);
    uint32_t windowSize = buffer.GetBits(16);
    uint64_t maxLength = 256;
    uint64_t bufferLength = maxLength * maxLength;
    if (!(windowSize == 32 * 1024 || windowSize == 3*1024)) {
        return;
    }
    std::deque<uint16_t> hz;
    std::deque<uint16_t> dictionary;
    std::string data = "";

    uint16_t byte1, byte2, byte3;
    while (!buffer.Empty()) {
        while (dictionary.size() > windowSize) {
            dictionary.pop_back();
        }
        byte1 = (uint16_t)buffer.GetBits(16);
        if (byte1 == 0) {
            byte1 = (uint16_t)buffer.GetBits(16);
            dictionary.push_front(byte1);
            data = byte1;
            output.write(data.data(), 1);
            continue;
        } else {
            byte2 = (uint16_t)buffer.GetBits(16);
            byte3 = (uint16_t)buffer.GetBits(16);
            for (uint16_t i = 0; i <= byte2; i++) {
                data = dictionary[byte1 - i];

                output.write(data.data(), 1);
                hz.push_back(dictionary[byte1-i]);
            }
            data = byte3;
            //output.write(data.data(), 1);



            for (uint16_t i = 0; i <= byte2; i++) {
                dictionary.push_front(hz.front());
                hz.pop_front();
            }
            //dictionary.push_front(byte3);
            hz.clear();
        }


    }

    dictionary.clear();
    hz.clear();
}
