//
// Created by mckenlly on 10/2/18.
//

#ifndef DA_KP_LZ77_HPP
#define DA_KP_LZ77_HPP


#include <string>

class LZ77 {
public:
    static void encode(std::istream &, std::ostream &,uint16_t windowSize = 32*1024, uint16_t maxLength = 256);
    static void decode(std::istream &, std::ostream &);

};


#endif //DA_KP_LZ77_HPP
